= Why Apache Arrow is important for Ruby

It is known for data processing world that Apache Arrow is
important. This talk shares why Apache Arrow is important especially
for Ruby community and how to make positive spiral. Also, this talk
introduces about Apache Arrow features Ruby community works on.

== For author

=== Show

  rake

=== Publish

  rake publish

== For viewers

=== Install

  gem install rabbit-slide-kou-the-data-thread

=== Show

  rabbit rabbit-slide-kou-the-data-thread.gem

